export class Station {
    constructor(
        public url: string,
        public title: string,
        public photo: string
    ) { }
}

