import { Station } from "../model/Station";

declare var chrome: any;
declare var window: any;

/**
 * Chromecast helper thing
 */
class Caster {

    public init() {
        window['__onGCastApiAvailable'] = () => {
            console.log('onGCastApiAvailable')
            const applicationID = "358F1E02";
            const sessionRequest = new chrome.cast.SessionRequest(applicationID);
            const apiConfig = new chrome.cast.ApiConfig(sessionRequest, function () {
            }, () => {
            });
            chrome.cast.initialize(apiConfig, () => {
                console.log("success")
            }, () => {
                console.log("failure")
            });
        };
    }

    public cast(station: Station) {
        chrome.cast.requestSession(function (session: any) {
            var mediaInfo = new chrome.cast.media.MediaInfo(station.url, 'audio/mpeg');

            var metadata = new chrome.cast.media.MusicTrackMediaMetadata();
            metadata.artist = station.title;
            metadata.title = station.title;
            var image = new chrome.cast.Image(station.photo);
            metadata.images = [image];

            mediaInfo.metadata = metadata;
            var request = new chrome.cast.media.LoadRequest(mediaInfo);
            request.autoplay = true;
            session.loadMedia(request, function () {
            }, () => {
                console.log('Error loading media')
            });
        }, () => {
            console.log('Error requesting session')
        });
    }
}


// Export a singleton instance in the global namespace
export const caster = new Caster();