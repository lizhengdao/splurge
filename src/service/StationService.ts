import { Station } from "../model/Station";

class StationService {

  getStations(): Station[] {
    const stations: Station[] = []
    const bbc1xtra = new Station(
      "http://bbcmedia.ic.llnwd.net/stream/bbcmedia_radio1xtra_mf_p",
      "1xtra",
      "http://ichef.bbci.co.uk/corporate2/images/width/live/p0/4h/bd/p04hbdjx.jpg/624"
    )
    const plaza = new Station(
      "https://plaza.one/mp3",
      "Nightwave Plaza",
      "https://d1itmove024qgx.cloudfront.net/718c8b65da97203e7752d7383ae8242996f97f1d.jpg"
    )
    const kissFM = new Station(
      "http://stream3.radio.is:443/kissfm",
      "Kiss FM",
      "http://www.kissfm.is/wp-content/uploads/2017/09/topplogo-2.png"
    )
    const chilledCow = new Station(
      "https://youtu.be/5qap5aO4i9A",
      "ChilledCow",
      "https://yt3.ggpht.com/a/AATXAJyWHijwsY8VMStodk3a9fhDxQamdI6ysP6CWg=s288-c-k-c0xffffffff-no-rj-mo"
    )
    const poolside = new Station(
      "https://poolside.fm/",
      "Poolside.fm",
      "https://poolside.fm/"
    )
    stations.push(bbc1xtra)
    stations.push(plaza)
    stations.push(kissFM)
    stations.push(chilledCow)
    stations.push(poolside)
    return stations
  }
}

// Export a singleton instance in the global namespace
export const stationService = new StationService()